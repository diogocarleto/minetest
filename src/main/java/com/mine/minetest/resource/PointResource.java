package com.mine.minetest.resource;


import com.mine.minetest.model.Coordinates;
import com.mine.minetest.proton.CoordinatesProto;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class PointResource {

    @RequestMapping(value = "calcShortestPathProto", method = RequestMethod.POST)
    public CoordinatesProto.Coordinates calcShortestPathProto(@RequestBody Coordinates coordinates) {
        List<Double[]> shortestPathList = getShortestPath(coordinates.getOrigin(), coordinates.getDestinations());

        CoordinatesProto.Coordinates.Builder cpBuider = CoordinatesProto.Coordinates.newBuilder();
        for(int i=0; i < shortestPathList.size(); i++) {
            Double[] destination = shortestPathList.get(i);
            CoordinatesProto.Coordinate coordinate = CoordinatesProto.Coordinate.newBuilder()
                    .addPoints(destination[0])
                    .addPoints(destination[1])
                    .setOrder(i+1).build();
            cpBuider.addCoordinate(coordinate);
        }

        return cpBuider.build();
    }

    @RequestMapping(value = "calcShortestPath", method = RequestMethod.POST)
    public List<Double[]> calcShortestPath(@RequestBody Coordinates coordinates) {
        return getShortestPath(coordinates.getOrigin(), coordinates.getDestinations());
    }

    private List<Double[]> getShortestPath(Double[] origin, Double[][] destinations) {
        List<Double[]> shortestPathDestinations = new ArrayList<>();

        //get the closestDestination, iterating over each element, applying the Euclidean distance algorithm.
        Double[] closestDestination = getClosestDestination(origin, destinations);
        shortestPathDestinations.add(closestDestination);

        //closestDestination is now the origin, what is only needed is to rebuild destinations
        destinations = rebuildDestinations(closestDestination, destinations);

        if (destinations.length > 1) {
            shortestPathDestinations.addAll(getShortestPath(closestDestination, destinations));
        } else {
            shortestPathDestinations.add(destinations[0]);
        }

        return shortestPathDestinations;
    }

    /**
     * Remove origin from the destinations.
     */
    private Double[][] rebuildDestinations(final Double[] origin, Double[][] destinations) {
        return Arrays.stream(destinations)
                .filter(destination -> !destination.equals(origin))
                .collect(Collectors.toList()).toArray(new Double[0][]);
    }

    /**
     * Get the closestDestination, iterating over each element, applying the Euclidean distance algorithm
     */
    private Double[] getClosestDestination(Double[] origin, Double[][] destinations) {
        Double[] closestDestination = null;
        Optional<Double> previousDistance = Optional.empty();

        for(int i=0; i < destinations.length; i++) {
            Double[] destination = destinations[i];
            double distance = new EuclideanDistance().compute(ArrayUtils.toPrimitive(origin), ArrayUtils.toPrimitive(destination));
            if (!previousDistance.isPresent() || previousDistance.get() > distance) {
                previousDistance = Optional.of(distance);
                closestDestination = destination;
            }
        }

        return closestDestination;
    }
}
