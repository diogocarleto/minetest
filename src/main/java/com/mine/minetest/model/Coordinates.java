package com.mine.minetest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Data
public class Coordinates {

    private Double[] origin;
    private Double[][] destinations;
//    private List<Coordinate> coordinate;
}
