package com.mine.minetest.resource;

import com.mine.minetest.MineTestApplication;
import com.mine.minetest.model.Coordinates;
import com.mine.minetest.proton.CoordinatesProto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MineTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PointResourceTests {

    @Configuration
    public static class RestClientConfiguration {
        @Bean
        ProtobufHttpMessageConverter protobufHttpMessageConverter() {
            return new ProtobufHttpMessageConverter();
        }
    }

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void calcShortestPathProto() {
        Coordinates coordinates = new Coordinates();
        coordinates.setOrigin(new Double[]{3.0, 4.0});
        coordinates.setDestinations(new Double[][]{
                {10.0, 3.0},
                {43.0, 0.0},
                {83.0, 2.0},
                {25.0, 5.0}
        });

        CoordinatesProto.Coordinates coordinatesProto = testRestTemplate.postForEntity(
                "/calcShortestPathProto",
                coordinates,
                CoordinatesProto.Coordinates.class).getBody();

        List<CoordinatesProto.Coordinate> coordinateList = coordinatesProto.getCoordinateList();
        assertThat(coordinatesProto.getCoordinateCount(), is(4));

        CoordinatesProto.Coordinate firstCoordinate = coordinateList.get(0);
        assertThat(firstCoordinate.getOrder(), is(1));
        assertThat(firstCoordinate.getPoints(0), is(10.0));
        assertThat(firstCoordinate.getPoints(1), is(3.0));
    }

    @Test
    public void calcShortestPath() {
        Coordinates coordinates = new Coordinates();
        coordinates.setOrigin(new Double[]{3.0, 4.0});
        coordinates.setDestinations(new Double[][]{
                {10.0, 3.0},
                {43.0, 0.0},
                {83.0, 2.0},
                {25.0, 5.0}
        });

        List<Double[]> coordinatesReturn = testRestTemplate.exchange(
                "/calcShortestPath",
                HttpMethod.POST,
                new HttpEntity<Coordinates>(coordinates),
                new ParameterizedTypeReference<List<Double[]>>() {}).getBody();

        assertThat(coordinatesReturn.size(), is(4));

        Double[] firstCoordinate = coordinatesReturn.get(0);
        assertThat(firstCoordinate[0], is(10.0));
        assertThat(firstCoordinate[1], is(3.0));
    }
}
